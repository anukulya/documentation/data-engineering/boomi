# Boomi - Integration platform as service (iPaaS)

## Getting started

![](assets/20240507_104838_image.png)

![](assets/20240507_110427_image.png)

![](assets/20240507_110447_image.png)

![](assets/20240507_110523_image.png)

![](assets/20240507_110636_image.png)

![](assets/20240507_110806_image.png)

![](assets/20240507_110905_image.png)

![](assets/20240507_111028_image.png)

### Build Tab

![](assets/20240507_111103_image.png)

![](assets/20240507_111227_image.png)

![](assets/20240507_111755_image.png)

![](assets/20240507_115748_image.png)

![](assets/20240507_121319_image.png)

![](assets/20240507_121400_image.png)

![](assets/20240507_121504_image.png)

![](assets/20240507_123629_image.png)

![](assets/20240507_123930_image.png)

![](assets/20240507_124021_image.png)

![](assets/20240507_124801_image.png)

![](assets/20240507_130109_image.png)

![](assets/20240507_130155_image.png)

![](assets/20240507_131114_image.png)

![](assets/20240507_131417_image.png)

![](assets/20240507_133904_image.png)

![](assets/20240507_143544_image.png)

![](assets/20240507_144812_image.png)

![](assets/20240507_145532_image.png)

### Deploy and Management Tab

![](assets/20240507_150009_image.png)

![](assets/20240507_150044_image.png)

![](assets/20240507_150141_image.png)

![](assets/20240507_150331_image.png)

![](assets/20240507_150408_image.png)

![](assets/20240507_151252_image.png)

![](assets/20240507_151332_image.png)

![](assets/20240507_151624_image.png)

![](assets/20240507_160917_image.png)

![](assets/20240507_160953_image.png)

![](assets/20240507_161039_image.png)

## Master Data Management | Master Data Hub

### Introduction

![](assets/20240510_110745_image.png)

![](assets/20240510_111059_image.png)

![](assets/20240510_111133_image.png)

![](assets/20240510_111510_image.png)

![](assets/20240510_112024_image.png)

![](assets/20240510_113346_image.png)

![](assets/20240510_113512_image.png)

![](assets/20240510_113908_image.png)

![](assets/20240510_114018_image.png)

![](assets/20240510_114058_image.png)

![](assets/20240510_114250_image.png)

![](assets/20240510_114455_image.png)

![](assets/20240510_114515_image.png)

![](assets/20240510_114535_image.png)

![](assets/20240510_114657_image.png)

![](assets/20240510_114928_image.png)

![](assets/20240510_115316_image.png)

![](assets/20240510_142554_image.png)

![](assets/20240510_142655_image.png)

![](assets/20240510_142807_image.png)

### Lifecycle - Define

![](assets/20240510_143106_image.png)

![](assets/20240510_143320_image.png)

![](assets/20240510_153242_image.png)

![](assets/20240510_153310_image.png)

![](assets/20240510_154449_image.png)

![](assets/20240510_154621_image.png)

### Lifecycle - Deploy

![](assets/20240510_160912_image.png)

![](assets/20240510_162251_image.png)

![](assets/20240510_162429_image.png)

![](assets/20240510_164314_image.png)

### Lifecycle - Synchronize

![](assets/20240510_164923_image.png)

In the below diagram, the `blue cog` refers to the Atomsphere process that establishes connection and data transfer between the source systems and the MDM platform.

![](assets/20240510_165631_image.png)

A quick walkthrough on concepts related to `Connector` and `Profile` of the Atomsphere platform.

![](assets/20240510_165823_image.png)

![](assets/20240510_165957_image.png)

![](assets/20240510_173704_image.png)

### Lifecycle - Steward

![](assets/20240513_111803_image.png)

![](assets/20240513_111825_image.png)

![](assets/20240513_111846_image.png)

![](assets/20240513_130313_image.png)

![](assets/20240513_133007_image.png)

## References

[What is Dell Boomi? - A Complete Tutorial](https://mindmajix.com/what-is-dell-boomi)

[Boomi Basic Full Video for Beginners](https://www.youtube.com/watch?v=S7YfIoBQL0M)

[Boomi Training Platform](https://community.boomi.com/s/training)

[Event Streams Release Highlights](https://players.brightcove.net/6261520393001/experience_64834bcb4deee4001a448425/share.html)

[BOOMI MDH | MDM](https://www.youtube.com/playlist?list=PLeD8jGNxnJXWvr0CJdEJZOmhtx7oTN5i6)
